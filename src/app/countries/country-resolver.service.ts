import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Country} from '../shared/country.model';
import {CountryService} from '../shared/country.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CountryResolverService implements Resolve<Country>{

  constructor(private countryService: CountryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const countryAlphaCode = <string>route.params['alpha3Code'];
    return this.countryService.fetchCountry(countryAlphaCode);
  }
}
