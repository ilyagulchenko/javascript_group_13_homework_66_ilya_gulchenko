import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Country} from '../../shared/country.model';
import {CountryInfo} from '../../shared/country-info.model';

@Component({
  selector: 'app-country-about',
  templateUrl: './country-about.component.html',
  styleUrls: ['./country-about.component.css']
})
export class CountryAboutComponent implements OnInit {
  country!: CountryInfo;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.country = <CountryInfo>data['country'];
    })
  }
}
