import {Component, OnDestroy, OnInit} from '@angular/core';
import {Country} from '../shared/country.model';
import {Subscription} from 'rxjs';
import {CountryService} from '../shared/country.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit, OnDestroy{
  countries: Country[] = [];
  countryChangeSubscriptions!: Subscription;
  countriesFetchingSubscriptions!: Subscription;
  isFetching = false;

  constructor(private countryService: CountryService) {}

  ngOnInit(): void {
    this.countryChangeSubscriptions = this.countryService.countriesFetch.subscribe((countries: Country[]) => {
      this.countries = countries;
    });
    this.countriesFetchingSubscriptions = this.countryService.countriesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.countryService.fetchCountries();
  }

  ngOnDestroy(): void {
    this.countryChangeSubscriptions.unsubscribe();
    this.countriesFetchingSubscriptions.unsubscribe();
  }
}
