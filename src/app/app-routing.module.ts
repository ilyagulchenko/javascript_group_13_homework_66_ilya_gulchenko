import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountriesComponent } from './countries/countries.component';
import { CountryResolverService } from './countries/country-resolver.service';
import { CountryAboutComponent } from './countries/country-about/country-about.component';

const routes: Routes = [
  {path: '', component: CountriesComponent},
  {path: 'countries', component: CountriesComponent, children: [
      {path: ':alpha3Code', component: CountryAboutComponent, resolve: {
        country: CountryResolverService
        }},
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
