export class CountryInfo {
  constructor(
    public name: string,
    public alpha3Code: string,
    public capital: string,
    public region: string,
    public population: number,
    public timezones: string,
    public nativeName: string,
    public flag: string
  ) {}
}
