import {Injectable} from '@angular/core';
import {map, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Country} from './country.model';
import {CountryInfo} from './country-info.model';

@Injectable()

export class CountryService {
  countriesFetch = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();

  private countries: Country[] = [];

  constructor(private http: HttpClient) { }

  fetchCountries() {
    this.countriesFetching.next(true);
    this.http.get<{[alpha3Code: string]: Country}>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const countryData = result[id];

          return new Country(
            countryData.name,
            countryData.alpha3Code,
          );
        });
      }))
      .subscribe(countries => {
        this.countries = countries;
        this.countriesFetch.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, error => {
        this.countriesFetching.next(false);
      });
  }

  fetchCountry(countryAlphaCode: string) {
    return this.http.get<CountryInfo>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${countryAlphaCode}`).pipe(
      map(result => {
        return new CountryInfo(result.name ,result.alpha3Code, result.capital, result.region, result.population, result.timezones, result.nativeName, result.flag);
      })
    )
  }
}
